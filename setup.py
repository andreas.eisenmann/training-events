#!/usr/bin/env python

from setuptools import setup, find_packages


# pylint: disable=invalid-name


install_requires = [
    'click>=7.0',
    'pytz',
    'requests',
    'tzlocal'
]

tests_require = [
    'coverage>=4.5.1',
    'diff-cover>=1.0.2',
    'pytest>=3.8.2',
    'pytest-cov>=2.6.0',
    'pytest-html>=1.19.0',
    'pylint>=2.1.1',
    'flake8>=3.5.0',
    'pytest-mock>=1.10.0',
    'requests-mock'
]

dev_requires = [
    'setuptools_scm>=3.1.0',
    'requirements-builder>=0.3.0'
]

if __name__ == "__main__":

    setup(
        name='training-events',
        description='',
        packages=find_packages("src"),
        package_dir={'': 'src'},
        entry_points={
            'console_scripts': ['training-events = cli:cli_main']
        },
        install_requires=install_requires,
        tests_require=tests_require,
        extras_require={
            'test': tests_require,
            'dev': dev_requires
        }
    )
