""" the main routine of this program, reads data from remote and writes to output """
from model import create_event_view
from read import read_users, read_areas, read_events
from write import write


def main(api_token, tenant_id, path_to_output_file):
    """Main entry point for the cli."""
    users_by_id = {user.id: user for user in read_users(api_token, tenant_id)}
    areas_by_id = {area.id: area for area in read_areas(api_token, tenant_id)}

    events = []
    for area_id in areas_by_id:
        events += read_events(api_token, area_id, tenant_id)

    def to_event_view(event):
        return create_event_view(event, users_by_id, areas_by_id)  # pragma: no cover

    event_views = map(to_event_view, events)

    write(path_to_output_file, event_views)
    print("export data written to {}".format(path_to_output_file))
