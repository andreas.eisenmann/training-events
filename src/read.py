""" read data from remote and return as entity objects """
from fetch import get_response
from from_json import to_user, to_area, to_events
from url import get_users_url, get_areas_url, get_events_url
from utils import flat_map


def read_users(api_token, tenant_id):
    """ fetch from remote and create users """
    users_url = get_users_url(tenant_id)
    response = get_response(users_url, api_token)
    users = map(to_user, response.json())
    return users


def read_areas(api_token, tenant_id):
    """ fetch from remote and create areas """
    areas_url = get_areas_url(tenant_id)
    response = get_response(areas_url, api_token)
    areas = map(to_area, response.json())
    return areas


def read_events(api_token, area_id, tenant_id):
    """ fetch from remote and create events """
    events_url = get_events_url(area_id, tenant_id)
    response = get_response(events_url, api_token)
    events = flat_map(to_events, response.json())
    return events
