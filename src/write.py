""" write objects to output files """
import csv

header = [
    "id",
    "title",
    "start",
    "end",
    "location",
    "responsibles",
    "num_participants",
    "areas",
    "cancelled",
]


def write(file_name, event_views):
    """ write to csv"""

    def event_view_to_line(event_view):
        return [
            event_view.id,
            event_view.title,
            event_view.start,
            event_view.end,
            event_view.location,
            event_view.responsibles,
            event_view.num_participants,
            event_view.areas,
            event_view.cancelled,
        ]

    lines = map(event_view_to_line, event_views)

    with open(file_name, "w", newline="", encoding="utf-8") as csvfile:
        writer = csv.writer(csvfile, dialect="excel")
        writer.writerow(header)
        for line in lines:
            writer.writerow(line)
