""" extract entity objects from given json"""
from datetime import datetime, timedelta
from uuid import uuid4
import pytz
from model import create_user, create_area, create_event


def to_user(json):
    """ convert from json to user """

    assert "userId" in json
    user_id = json.get("userId")

    first_name = ""
    last_name = ""
    if "name" in json:
        name = json.get("name")
        if "firstName" in name:
            first_name = name.get("firstName")
        if "lastName" in name:
            last_name = name.get("lastName")

    return create_user(user_id, first_name=first_name, last_name=last_name)


def to_area(json):
    """ convert from json to area """

    assert "areaId" in json
    area_id = json.get("areaId")

    name = ""
    if "name" in json:
        name = json.get("name")

    return create_area(area_id, name=name)


# TODO: resolve pylint warnings
# pylint: disable=too-many-locals, too-many-branches, too-many-statements
def to_events(json):
    """ convert from json to event """
    title = ""
    if "title" in json:
        title = json.get("title")

    location_name = ""
    responsible_ids_by_event = []
    if "eventDetails" in json:
        event_details = json.get("eventDetails")
        if "location" in event_details:
            location = event_details.get("location")
            if "name" in location:
                location_name = location.get("name")

        if "responsibleUsers" in event_details:
            responsible_ids_by_event = event_details.get("responsibleUsers")

    responsible_ids_by_instance = {}
    cancelled_instances = []
    if "instanceDetails" in json:
        instance_details = json.get("instanceDetails")
        if "instanceUpdates" in instance_details:
            instance_updates = instance_details.get("instanceUpdates")
            for instance in instance_updates:
                if "responsibleUsers" in instance_updates.get(instance):
                    responsible_ids_by_instance[instance] = instance_updates.get(
                        instance
                    ).get("responsibleUsers")
        if "instanceCancellations" in instance_details:
            instance_cancellations = instance_details.get("instanceCancellations")
            for instance in instance_cancellations:
                cancelled_instances.append(instance)

    start = None
    if "startDateTime" in json:
        start = json.get("startDateTime")
        start = start[:19]
        start = datetime.strptime(start, "%Y-%m-%dT%H:%M:%S")

    end = None
    if "endDateTime" in json:
        end = json.get("endDateTime")
        end = end[:19]
        end = datetime.strptime(end, "%Y-%m-%dT%H:%M:%S")

    duration = timedelta(0)
    if start and end:
        duration = end - start

    area_ids = []
    if "context" in json:
        context = json.get("context")
        if "areaIds" in context:
            area_ids = context.get("areaIds")

    series_cancelled = False
    if "cancelled" in json:
        series_cancelled = json.get("cancelled")

    if "commitments" not in json:
        return

    commitments = json.get("commitments")

    for commitment in commitments:
        start = datetime.fromtimestamp(float(commitment), tz=pytz.utc)
        end = start + duration

        responsible_ids = responsible_ids_by_instance.get(commitment)
        if not responsible_ids:
            responsible_ids = responsible_ids_by_event

        cancelled = commitment in cancelled_instances or series_cancelled

        num_participants = 0
        if "participants" in commitments.get(commitment):
            num_participants = len(commitments.get(commitment).get("participants"))

        event = create_event(
            str(uuid4()),
            title=title,
            start=start,
            end=end,
            location=location_name,
            responsible_ids=responsible_ids,
            num_participants=num_participants,
            area_ids=area_ids,
            cancelled=cancelled,
        )
        yield event
