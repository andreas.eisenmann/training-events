""" utilities """


def flatten(list_of_lists):
    """ flatten items from lists in lists into one common list """
    return iter([item for list in list_of_lists for item in list])


def flat_map(func, iterable):
    """ return flattened items after applying map function """
    return flatten(map(func, iterable))
