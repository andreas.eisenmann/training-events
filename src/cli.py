"""The command line interface."""
import os
import click
from main import main


@click.command()
@click.option(
    "--api-token", "-a", required=True, help="X-ATUH api token to authenticate"
)
@click.option("--tenant-id", "-t", required=True, help="the Klubraum tenant id")
@click.option(
    "--output-file",
    "-o",
    "path_to_output_file",
    required=True,
    help="path to the output file",
)
@click.option(
    "--timezone", "-tz", required=False, help="the local timezone for dates and times"
)
def cli_main(api_token, tenant_id, path_to_output_file, timezone):
    """Main entry point for the cli."""

    if timezone:
        os.environ["TZ"] = timezone

    main(api_token, tenant_id, path_to_output_file)
