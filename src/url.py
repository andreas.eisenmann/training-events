""" determine the urls to fetch from """

KLUBRAUM_BASE_URL = "https://api.klubraum.com/api-v1"


def get_users_url(tenant_id):
    """ get the url to fetch users """
    return "{base_url}/user/list?tenantId={tenant_id}".format(
        base_url=KLUBRAUM_BASE_URL, tenant_id=tenant_id
    )


def get_areas_url(tenant_id):
    """ get the url to fetch areas """
    return "{base_url}/metadata/area/list?tenantId={tenant_id}".format(
        base_url=KLUBRAUM_BASE_URL, tenant_id=tenant_id
    )


def get_events_url(area_id, tenant_id):
    """ get the url to fetch events """
    return "{base_url}/event/public/area/{area_id}/list?tenantId={tenant_id}".format(
        base_url=KLUBRAUM_BASE_URL, area_id=area_id, tenant_id=tenant_id
    )
