""" fetch responses for given urls """
import requests


def get_response(url, api_token):
    """ return a response for a given url """
    headers = {
        "Accept": "application/json",
        "X-Auth-Token": api_token,
    }
    response = requests.get(url, headers=headers)
    return response
