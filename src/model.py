""" provide constructors for model entitites """
from uuid import uuid4
from tzlocal import get_localzone


def create_user(id_, first_name=None, last_name=None):
    """ create a user """
    user = DotDict(key=id_)
    # id (uuid): id of the user
    user.id = id_
    # first_name (str): first name of the user
    user.first_name = first_name
    # last_name (str): last name of the user
    user.last_name = last_name

    return user


def create_area(id_, name=None):
    """ create an area """
    area = DotDict(key=id_)
    # id (uuid): id of the area
    area.id = id_
    # name (str): name of the area
    area.name = name

    return area


# pylint: disable=too-many-arguments
def create_event(
    id_,
    title=None,
    start=None,
    end=None,
    location=None,
    responsible_ids=None,
    num_participants=0,
    area_ids=None,
    cancelled=False,
):
    """ create an event """
    event = DotDict(key=id_)
    # id (uuid): id of the area
    event.id = id_
    # title (str): title of the event
    event.title = title
    # start (datetime): start of the event
    event.start = start
    # end (datetime): end of the event
    event.end = end
    # location (str): location of the event
    event.location = location
    # responsible_ids list(str): list of repsonsible user ids
    event.responsible_ids = responsible_ids or []
    # num_participants int: number of participants
    event.num_participants = num_participants
    # area_ids list(str): list of area ids
    event.area_ids = area_ids or []
    # cancelled bool: whether the event has been cancelled
    event.cancelled = cancelled

    return event


def create_event_view(
    event, users_by_id, areas_by_id,
):
    """ create a view on an event with user and area names """
    event_view = DotDict(key=event.id)
    event_view.id = event.id
    event_view.title = event.title
    if event.start:
        event_view.start = event.start.astimezone(get_localzone()).strftime(
            "%d.%m.%Y %H:%M"
        )
    if event.end:
        event_view.end = event.end.astimezone(get_localzone()).strftime(
            "%d.%m.%Y %H:%M"
        )
    event_view.location = event.location
    users = [
        users_by_id.get(responsible_id)
        for responsible_id in event.responsible_ids
        if responsible_id in users_by_id
    ]
    event_view.responsibles = ", ".join(
        [
            "{first_name} {last_name}".format(
                first_name=user.first_name, last_name=user.last_name
            )
            for user in users
        ]
    )
    event_view.num_participants = event.num_participants
    areas = [
        areas_by_id.get(area_id) for area_id in event.area_ids if area_id in areas_by_id
    ]
    event_view.areas = ", ".join(
        ["{area_name}".format(area_name=area.name) for area in areas]
    )
    event_view.cancelled = event.cancelled

    return event_view


# pylint: disable=too-many-instance-attributes
class DotDict(dict):
    """
    utilitiy class to serve as generic data class
    based on dictionary and supports dot notation
    as well as dictionary access notation

    usage: d = DotDict() or d = DotDict({'val1':'first'})
    set attributes:
    d.val2 = 'second' or d['val2'] = 'second'
    get attributes:
    d.val2 or d['val2']

    supports equality check via the "key" argument
    """

    def __getattr__(self, key):
        try:
            return dict.__getitem__(self, key)
        except KeyError:
            return None

    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

    def __init__(self, key=None):
        """
        Args:
            key (str):  unique key identifyer
        """
        if not key:
            self._key = uuid4()
        else:
            self._key = key

        super().__init__({})

    def __eq__(self, other):
        """check for equality"""
        if not isinstance(other, self.__class__):
            return False

        if self._key == other._key:
            return True

        return False

    def __ne__(self, other):
        """check for inequality"""
        return not self == other

    def __lt__(self, other):
        return self._key < other._key

    def __hash__(self):
        return hash(self._key)
