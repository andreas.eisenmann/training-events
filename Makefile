ifndef VIRTUAL_ENV
pip = $(error "No active virtualenv for pip. You most likely don't want to install any system-wide packages.")
pytest = $(error "No active virtualenv for pytest. You most likely don't want to install any system-wide packages.")
else
pip = pip
pytest = pytest $(PYTEST_ARGS)
endif

define ERROR
	(echo -en "\nERROR: [$@]: $(1)\n\n" && exit 1)
endef

PYLINT_PROCESSES = 5

VERSION = $(shell python setup.py --version)
PYTHON_FILES := $(shell find src tests -name '*.py')

# create a python virtualenv

venv:
	@echo "Creating pre-configured virtualenv ..."
	virtualenv -p python3 --no-download ./venv
	./venv/bin/pip install --upgrade pip setuptools wheel
	@echo "Use 'source venv/bin/activate' to activate the virtualenv."


init.marker: setup.py setup.cfg
	$(pip) install -e .[dev] && touch init.marker
init: init.marker
.PHONY: init

# initialize test requirements
init-tests.marker: init.marker setup.py
	@echo "Setting up test environment ..."
	$(pip) install -e .[test] && touch init-tests.marker
init-tests: init-tests.marker
.PHONY: init-tests

format: init
	@echo "Running Black format checker ..."
	@black $(PYTHON_FILES)
.PHONY: format

define FLAKE8
	@echo "Running flake8 code checks ..."
	@flake8 $(1) || $(call ERROR, "Flake8 static code checks failed!")
endef

flake8: init-tests.marker
	$(call FLAKE8,$(PYTHON_FILES))
.PHONY: flake8

define PYLINT
	@echo "Running pylint ..."
	@pylint --rcfile=setup.cfg --reports=no -f msvs -j $(PYLINT_PROCESSES) $(1) || $(call ERROR, "Pylint static code checks failed!")
endef

pylint: init-tests.marker
	$(call PYLINT,$(PYTHON_FILES))

check: format flake8 pylint test
.PHONY: check

# run all tests
test: init-tests.marker init.marker
	@echo "Running pytest"
	@$(pytest) .
.PHONY: test

# run code coverage
coverage: PYTEST_ARGS := $(PYTEST_ARGS) $(COVERAGE_PYTEST_ARGS)
coverage: init-tests.marker
	@echo "Running tests collecting code coverage ..."
	@coverage run --branch --source src -m $(pytest)
	@echo "Generating coverage report ..."
	@coverage html $(COV_FLAGS)
	@coverage xml $(COV_FLAGS)
	@coverage report --show-missing --skip-covered --fail-under=90 $(COV_FLAGS) || $(call ERROR, "Overall code coverage below 90%!")
	@echo "Collecting diff coverage ..."
	@diff-cover coverage.xml --html-report htmlcov/report.html --fail-under 90 || $(call ERROR, "Diff code coverage below 90%!")
.PHONY: coverage


clean:
	@rm -rfv build/ dist/ docs/_build/ *.marker testreport/ htmlcov/ src/*.egg-info
.PHONY: clean


install.marker: init.marker
	$(pip) install -e . && touch install.marker
install: install.marker
.PHONY: install
