""" test the cli module """
import pytest

from cli import cli_main


@pytest.mark.parametrize(
    "additional_args", [[], ["-tz", "Europe/Berlin"]],
)
def test_cli_main(mocker, additional_args):
    """ simple test that runs main """
    args = [
        "dummy-executable",
        "-a",
        "some_token",
        "-t",
        "some_tenant",
        "-o",
        "events.csv",
    ]
    args.extend(additional_args)
    mocker.patch(
        "sys.argv", args,
    )
    mocker.patch("cli.main")

    try:
        # pylint: disable=no-value-for-parameter
        cli_main()
    except SystemExit as exit_err:
        assert exit_err.code == 0
