""" test the read module """

from read import read_users, read_areas, read_events

# pylint: disable=too-many-arguments


def test_read_users(
    mocker,
    api_token,
    tenant_id,
    create_response,
    user_id,
    first_name,
    last_name,
    user_as_json,
):
    """ test reading the users"""
    response = create_response(json=[user_as_json])
    mocker.patch("read.get_users_url")
    mocker.patch("read.get_response", return_value=response)
    user = next(read_users(api_token, tenant_id))
    assert user.id == user_id
    assert user.first_name == first_name
    assert user.last_name == last_name


def test_read_areas(
    mocker, api_token, tenant_id, create_response, area_id, area_name, area_as_json
):
    """ test reading the areas"""
    response = create_response(json=[area_as_json])
    mocker.patch("read.get_areas_url")
    mocker.patch("read.get_response", return_value=response)
    area = next(read_areas(api_token, tenant_id))
    assert area.id == area_id
    assert area.name == area_name


def test_read_events(
    mocker,
    api_token,
    tenant_id,
    create_response,
    area_id,
    event_title,
    event_start,
    event_end,
    event_location,
    events_as_json,
):
    """ test reading the events"""
    response = create_response(json=[events_as_json])
    mocker.patch("read.get_events_url")
    mocker.patch("read.get_response", return_value=response)
    events = read_events(api_token, area_id, tenant_id)
    event = next(events)
    assert event.id
    assert event.title == event_title
    assert event.start == event_start
    assert event.end == event_end
    assert event.location == event_location
