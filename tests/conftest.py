""" module for pytest fixtures """
from datetime import datetime
from uuid import uuid4
import pytest
import pytz

from model import create_event, create_area, create_user, create_event_view

# pylint: disable=redefined-outer-name, too-many-arguments


@pytest.fixture
def tenant_id():
    """Return a tenant id """
    return str(uuid4())


@pytest.fixture
def api_token():
    """Return an api_token """
    return str(uuid4())


@pytest.fixture
def create_response():
    """Return a function that creates a response """

    class Response:
        """ helper class to mock a requests.Response """

        def __init__(self, json=None, url=None, reason=None, status_code=None):
            self._json = json
            self.url = url
            self.reason = reason
            self.status_code = status_code

        def json(self):
            """ mock the json() attribute """
            if not self._json or self._json == "":
                raise ValueError
            return self._json

        def raise_for_status(self):
            """ mock the raise_for_status() method """

    def create(json=None, url=None, reason=None, status_code=None):
        return Response(json=json, url=url, reason=reason, status_code=status_code)

    return create


@pytest.fixture
def user_id():
    """Return a user id """
    return str(uuid4())


@pytest.fixture
def first_name():
    """Return a user's first name """
    return "first name"


@pytest.fixture
def last_name():
    """Return a user's last name """
    return "last name"


@pytest.fixture
def user(user_id, first_name, last_name):
    """Return a user """
    return create_user(user_id, first_name=first_name, last_name=last_name)


@pytest.fixture
def area_id():
    """Return an area id """
    return str(uuid4())


@pytest.fixture
def area_name():
    """Return an area name """
    return "area name"


@pytest.fixture
def area(area_id, area_name):
    """Return an area """
    return create_area(area_id, name=area_name)


@pytest.fixture
def event_id():
    """Return an event id """
    return str(uuid4())


@pytest.fixture
def event_title():
    """Return an event title """
    return "event title"


@pytest.fixture
def event_start():
    """Return a start date of the event """
    return datetime.now().replace(microsecond=0, tzinfo=pytz.utc)


@pytest.fixture
def event_end():
    """Return an end date of the event """
    return datetime.now().replace(microsecond=0, tzinfo=pytz.utc)


@pytest.fixture
def event_location():
    """Return a location of the event """
    return "my location"


@pytest.fixture
def responsible_ids():
    """Return a list of responsible user ids of the event """
    return ["1", "2"]


@pytest.fixture
def num_participants():
    """Return a number of participants """
    return 5


@pytest.fixture
def event(
    event_id,
    event_title,
    event_start,
    event_end,
    event_location,
    responsible_ids,
    num_participants,
    area_id,
):
    """ return an event """
    return create_event(
        event_id,
        title=event_title,
        start=event_start,
        end=event_end,
        location=event_location,
        responsible_ids=responsible_ids,
        num_participants=num_participants,
        area_ids=[area_id],
        cancelled=True,
    )


@pytest.fixture
def user_as_json(user_id, first_name, last_name):
    """Return a JSON from user data """
    return {"userId": user_id, "name": {"firstName": first_name, "lastName": last_name}}


@pytest.fixture
def area_as_json(area_id, area_name):
    """Return a JSON from area data """
    return {"areaId": area_id, "name": area_name}


@pytest.fixture
def events_as_json(event_id, event_title, event_start, event_end, event_location):
    """Return a JSON from event data """
    return {
        "eventId": event_id,
        "title": event_title,
        "startDateTime": event_start.isoformat(),
        "endDateTime": event_end.isoformat(),
        "eventDetails": {"location": {"name": event_location}},
        "commitments": {
            str(int(event_start.timestamp())): {"participants": [], "decliners": []}
        },
    }


@pytest.fixture
def output_file(tmpdir):
    """Return an empty temporary csv output file """
    return tmpdir.join("events.csv")


@pytest.fixture
def event_view(event, user, area):
    """Return an event view """
    users_by_id = {user.id: user}
    areas_by_id = {area.id: area}
    event.responsible_ids.append(user.id)
    event.area_ids.append(area.id)

    return create_event_view(event, users_by_id, areas_by_id)
