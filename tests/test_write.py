""" test the write module """
from write import write, header


def test_write(output_file, event_view):
    """ test write """
    write(output_file.strpath, [event_view])

    content = output_file.read()
    for item in header:
        assert item in content

    for value in event_view.values():
        assert str(value) in content
