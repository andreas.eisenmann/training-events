""" test the model module """

from model import create_user, create_area, create_event, create_event_view, DotDict


def test_create_user(user_id, first_name, last_name):
    """test creating a user"""
    user = create_user(user_id, first_name=first_name, last_name=last_name)
    assert user.id == user_id
    assert user.first_name == first_name
    assert user.last_name == last_name


def test_user_equals(user_id):
    """test equality of users"""
    user1 = create_user(user_id)
    user2 = create_user(user_id)
    user3 = create_user(5)
    assert user1 == user2
    assert user1 != user3


def test_create_area(area_id, area_name):
    """test creating an area"""
    area = create_area(area_id, name=area_name)
    assert area.id == area_id
    assert area.name == area_name


def test_area_equals(area_id):
    """test equality of areas"""
    area1 = create_area(area_id)
    area2 = create_area(area_id)
    area3 = create_area(5)
    assert area1 == area2
    assert area1 != area3


# pylint: disable=too-many-arguments
def test_create_event(
    event_id,
    event_title,
    event_start,
    event_end,
    event_location,
    responsible_ids,
    num_participants,
    area_id,
):
    """test creating an event"""
    event = create_event(
        event_id,
        title=event_title,
        start=event_start,
        end=event_end,
        location=event_location,
        responsible_ids=responsible_ids,
        num_participants=num_participants,
        area_ids=[area_id],
        cancelled=True,
    )
    assert event.id == event_id
    assert event.title == event_title
    assert event.start == event_start
    assert event.end == event_end
    assert event.location == event_location
    assert event.responsible_ids == responsible_ids
    assert event.num_participants == num_participants
    assert event.area_ids == [area_id]
    assert event.cancelled


def test_event_equals(event_id):
    """test equality of events"""
    event1 = create_event(event_id)
    event2 = create_event(event_id)
    event3 = create_event(5)
    assert event1 == event2
    assert event1 != event3


def test_create_event_view(event, user, area):
    """test creating an event view"""
    users_by_id = {user.id: user}
    areas_by_id = {area.id: area}
    event.responsible_ids.append(user.id)
    event.area_ids.append(area.id)

    event_view = create_event_view(event, users_by_id, areas_by_id)

    assert event_view.id == event.id
    assert event_view.title == event.title
    assert event_view.start
    assert event_view.end
    assert event_view.location == event.location
    assert event_view.num_participants == event.num_participants
    assert event_view.cancelled == event.cancelled

    assert area.name in event_view.areas
    assert user.first_name in event_view.responsibles
    assert user.last_name in event_view.responsibles


def test_create_event_view_no_dates(event):
    """test creating an event view"""
    event.start = None
    event.end = None

    event_view = create_event_view(event, {}, {})

    assert not event_view.start
    assert not event_view.end


def test_dotdict():
    """ test DotDict """
    assert not DotDict().a

    dotdict1 = DotDict()
    dotdict1.a = True
    assert dotdict1.a


def test_dotdict_equality():
    """ test DotDict equality"""
    dotdict1 = DotDict(key=1)
    dotdict2 = DotDict(key=1)
    dotdict3 = DotDict(key=2)
    assert dotdict1 == dotdict2
    assert dotdict1 != dotdict3
    assert dotdict1 != "a"
    assert dotdict1.__hash__() == dotdict2.__hash__()


def test_dotdict_compare():
    """ test DotDict comparison"""
    dotdict1 = DotDict(key=1)
    dotdict2 = DotDict(key=2)
    assert dotdict1 < dotdict2
