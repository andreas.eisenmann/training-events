""" test the from_json module """

from from_json import to_user, to_area, to_events

# pylint: disable=too-many-arguments


def test_to_user(user_id, first_name, last_name, user_as_json):
    """ test extracting a user from JSON """
    user = to_user(user_as_json)
    assert user.id == user_id
    assert user.first_name == first_name
    assert user.last_name == last_name


def test_to_user_no_name(user_id):
    """ test extracting a user from JSON """
    user = to_user({"userId": user_id})
    assert user.id == user_id
    assert not user.first_name
    assert not user.last_name


def test_to_user_no_firstname(user_id, last_name):
    """ test extracting a user from JSON """
    user = to_user({"userId": user_id, "name": {"lastName": last_name}})
    assert user.id == user_id
    assert not user.first_name
    assert user.last_name


def test_to_user_no_lastname(user_id, first_name):
    """ test extracting a user from JSON """
    user = to_user({"userId": user_id, "name": {"firstName": first_name}})
    assert user.id == user_id
    assert user.first_name
    assert not user.last_name


def test_to_area(area_id, area_name, area_as_json):
    """ test extracting an area from JSON """
    area = to_area(area_as_json)
    assert area.id == area_id
    assert area.name == area_name


def test_to_area_no_name(area_id):
    """ test extracting an area from JSON """
    area = to_area({"areaId": area_id})
    assert area.id == area_id
    assert not area.name


def test_to_events(event_title, event_start, event_end, event_location, events_as_json):
    """ test extracting an event from JSON """
    events = to_events(events_as_json)
    event = next(events)
    assert event.id
    assert event.title == event_title
    assert event.start == event_start
    assert event.end == event_end
    assert event.location == event_location


def test_to_events_no_data(event_id):
    """ test extracting an event from JSON """
    events = to_events({"eventId": event_id})
    assert not list(events)


def test_to_events_no_location(event_id):
    """ test extracting an event from JSON """
    events = to_events(
        {"eventId": event_id, "eventDetails": {}, "commitments": {"1": {}}}
    )
    event = next(events)
    assert not event.location


def test_to_events_no_location_name(event_id):
    """ test extracting an event from JSON """
    events = to_events(
        {
            "eventId": event_id,
            "eventDetails": {"location": {}},
            "commitments": {"1": {}},
        }
    )
    event = next(events)
    assert not event.location


def test_to_events_no_commitments(event_id):
    """ test extracting an event from JSON """
    events = to_events({"eventId": event_id, "commitments": {}})
    assert not list(events)


def test_to_events_responsible1(event_id, responsible_ids):
    """ test extract responsible from event details  """
    events = to_events(
        {
            "eventId": event_id,
            "eventDetails": {"responsibleUsers": responsible_ids},
            "commitments": {"1": {}},
        }
    )
    event = next(events)
    assert event.responsible_ids == responsible_ids


def test_to_events_responsible2(event_id, responsible_ids):
    """ test extract responsible from instance details  """
    events = to_events(
        {
            "eventId": event_id,
            "commitments": {"1": {}},
            "instanceDetails": {
                "instanceUpdates": {"1": {"responsibleUsers": responsible_ids}}
            },
        }
    )
    event = next(events)
    assert event.responsible_ids == responsible_ids


def test_to_events_responsible3(event_id, responsible_ids):
    """ test extract responsible from instance details  """
    events = to_events(
        {
            "eventId": event_id,
            "commitments": {"1": {}},
            "eventDetails": {"responsibleUsers": ["33", "44"]},
            "instanceDetails": {
                "instanceUpdates": {"1": {"responsibleUsers": responsible_ids}}
            },
        }
    )
    event = next(events)
    assert event.responsible_ids == responsible_ids


def test_to_events_responsible4(event_id, responsible_ids):
    """ test extract responsible from instance details  """
    events = to_events(
        {
            "eventId": event_id,
            "commitments": {"1": {}},
            "eventDetails": {"responsibleUsers": responsible_ids},
            "instanceDetails": {},
        }
    )
    event = next(events)
    assert event.responsible_ids == responsible_ids


def test_to_events_responsible5(event_id, responsible_ids):
    """ test extract responsible from instance details  """
    events = to_events(
        {
            "eventId": event_id,
            "commitments": {"1": {}},
            "eventDetails": {"responsibleUsers": responsible_ids},
            "instanceDetails": {"instanceUpdates": {"1": {}}},
        }
    )
    event = next(events)
    assert event.responsible_ids == responsible_ids


def test_to_events_num_participants(event_id):
    """ test extract number of participamts from instance details """
    events = to_events(
        {
            "eventId": event_id,
            "commitments": {"1": {"participants": [1, 2, 3]}},
            "instanceDetails": {"instanceUpdates": {"1": {}}},
        }
    )
    event = next(events)
    assert event.num_participants == 3


def test_to_events_area_ids1(event_id):
    """ test extract the area id from json """
    events = to_events({"eventId": event_id, "commitments": {"1": {}}})
    event = next(events)
    assert not event.area_ids


def test_to_events_area_ids2(event_id):
    """ test extract the area id from json """
    events = to_events({"eventId": event_id, "commitments": {"1": {}}, "context": {}})
    event = next(events)
    assert not event.area_ids


def test_to_events_area_ids3(event_id, area_id):
    """ test extract the area id from json """
    events = to_events(
        {
            "eventId": event_id,
            "commitments": {"1": {}},
            "context": {"areaIds": [area_id]},
        }
    )
    event = next(events)
    assert area_id in event.area_ids


def test_to_events_cancelled1(event_id):
    """ test extract whether event is cancelled """
    events = to_events(
        {
            "eventId": event_id,
            "commitments": {"1": {}},
            "instanceDetails": {"instanceCancellations": {"1": {}}},
        }
    )
    event = next(events)
    assert event.cancelled


def test_to_events_cancelled2(event_id):
    """ test extract whether event is cancelled """
    events = to_events(
        {
            "eventId": event_id,
            "commitments": {"1": {}},
            "instanceDetails": {"instanceCancellations": {}},
        }
    )
    event = next(events)
    assert not event.cancelled


def test_to_events_cancelled3(event_id):
    """ test extract whether event is cancelled """
    events = to_events(
        {"eventId": event_id, "commitments": {"1": {}}, "instanceDetails": {}}
    )
    event = next(events)
    assert not event.cancelled


def test_to_events_cancelled4(event_id):
    """ test extract whether event is cancelled """
    events = to_events(
        {"eventId": event_id, "commitments": {"1": {}}, "cancelled": True}
    )
    event = next(events)
    assert event.cancelled
