""" test the write module """
from utils import flatten, flat_map


def test_flatten():
    """ test utils.flatten """
    results = flatten([[1], [2]])
    assert next(results) == 1
    assert next(results) == 2


def test_flat_map():
    """ test utils.flat_map """

    def duplicate(num):
        return [num, num + 1]

    results = flat_map(duplicate, [1, 10])

    assert next(results) == 1
    assert next(results) == 2
    assert next(results) == 10
    assert next(results) == 11
