""" test the fetch module """

from fetch import get_response
from url import get_users_url


def test_fetch(requests_mock, tenant_id, api_token):
    """ test fetching some content from an url """
    url = get_users_url(tenant_id)
    requests_mock.get(url, json={"name": "klubraum"})
    response = get_response(url, api_token)
    assert "klubraum" in response.text
