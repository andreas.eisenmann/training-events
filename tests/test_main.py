""" test the main module """
from main import main


def test_main(mocker, user, area, event):
    """ test that runs main """
    mocker.patch("main.read_users", return_value=iter([user]))
    mocker.patch("main.read_areas", return_value=iter([area]))
    mocker.patch("main.read_events", return_value=iter([event]))
    mocker.patch("main.create_event_view")
    mocker.patch("main.write")

    main(None, None, None)
