""" test the url module """
from url import get_users_url, get_areas_url, get_events_url, KLUBRAUM_BASE_URL


def test_get_users_url(tenant_id):
    """ test determining the user list url """
    users_url = get_users_url(tenant_id)

    assert KLUBRAUM_BASE_URL in users_url
    assert "user/list?" in users_url
    assert tenant_id in users_url


def test_get_areas_url(tenant_id):
    """ test determining the area list url """
    areas_url = get_areas_url(tenant_id)

    assert KLUBRAUM_BASE_URL in areas_url
    assert "metadata/area/list?" in areas_url
    assert tenant_id in areas_url


def test_get_events_url(area_id, tenant_id):
    """ test determining the event list url """
    events_url = get_events_url(area_id, tenant_id)

    assert KLUBRAUM_BASE_URL in events_url
    assert "event/public/area" in events_url
    assert area_id in events_url
    assert tenant_id in events_url
