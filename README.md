# training-events

## install and activate the python virtual environment

$ make venv
$ source venv/bin/activate

## install the app

$ make install

## run the app

$ training-events --api-token `<x-auth-token>` --tenant-id `<tenant-id>` --output-file `<path-to-output-file>` [--timezone "`<my-local-timezone>`"]
